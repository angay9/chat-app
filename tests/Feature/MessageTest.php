<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class MessageTest extends TestCase
{
	// use DatabaseMigrations;
	use DatabaseTransactions;
	use WithoutMiddleware;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_a_user_can_send_a_message()
    {
        $this->login();
        $body = 'Hello';

        $response = $this->postMessage($body);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'success'	=>	true
            ])
        ;

        $this->assertDatabaseHas('messages', [
        	'body'	=>	$body
        ]);
    }

    public function test_only_authenticated_user_can_send_a_message()
    {
    	$body = 'Testing if authenticated';

    	$response = $this->postMessage($body);

    	$response->assertStatus(401);

    	$this->assertDatabaseMissing('messages', [
    		'body'	=>	$body
    	]);
    }

    protected function postMessage($body)
    {
    	return $this->json('POST', '/messages', [
	        	'channelId'	=>	1,
	        	'message'	=>	$body
	        ])
    	;
    }
}

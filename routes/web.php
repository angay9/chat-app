<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->middleware('auth');

// Route::post('/users', 'UsersController@store');
// Route::delete('/users/delete', 'UsersController@destroy');

Route::post('/messages', 'MessagesController@store');
Route::get('/login', function () {
    
        return view('auth.login');
    })
    ->name('login')
;

Route::get('/logout', 'UsersController@logout')->middleware('auth');

Route::get('/oauth/login', 'UsersController@redirectToProvider')
    // ->name('login')
    ->middleware('guest')
;
Route::get('/oauth/callback', 'UsersController@handleProviderCallback');

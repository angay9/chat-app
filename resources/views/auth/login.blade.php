@extends('layout', ['channels' => collect([])])
@section('content')

    <div class="container container--center">
        <div class="has-text-centered">
            <h2>Login</h2>
            <a href="/oauth/login" class="button is-info">
                <i class="fa fa-facebook"></i> &nbsp;
                Login using Facebook
            </a>
        </div>
    </div>
@stop
@extends('layout', ['channels' => collect([])])
@section('content')
    <div class="container container--centered has-text-centered">
        <h2 class="title is-3">
            You have been logged out. Come back as soon as you can or <a href="/login">login</a> again :)
        </h2>
        <a href=""></a>
    </div>
@stop
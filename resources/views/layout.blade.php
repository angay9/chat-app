<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/app.css">

        <script>
            
            window.Laravel = {
                csrfToken: "{{ csrf_token() }}",
                user: {!! Auth::check() ? Auth::user() : "null" !!},
                channels: {!! $channels !!}
            };

        </script>
    </head>
    <body>
        <div id="app" class="wrapper">
            @yield('content')
        </div>
    </body>

    <script src="/js/app.js"></script>
</html>

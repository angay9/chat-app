@extends('layout')
@section('content')
    <aside class="sidebar">
        <div class="sidebar__channels">
            <h3 class="sidebar__title is-clearfix">
                Channels
                <a href="/logout" class="button is-pulled-right">
                    <i class="fa fa-sign-out"></i>
                    Logout
                </a>
            </h3>
            <ul class="channel__list channel__list--pushed">
                <li v-for="(channel, index) in channels" class="channel__item">
                    <a class="channel__item__link" :class="{'channel__item__link--active': selectedChannel.id == channel.id}" href="#" @click.prevent="selectChannel(channel)">
                        @{{ channel.name }} &nbsp;&nbsp; <span class="tag is-pulled-right is-info">@{{ channel.messages.length }}</span>
                    </a>
                </li>
            </ul>
        </div>
    </aside>

    <div class="main">
        <div v-if="selectedChannel && user" class="messages">
            <ul class="messages__list" v-if="selectedChannel.messages.length">
                <li 
                    class="messages__list__item"
                    v-for="message in selectedChannel.messages"
                    :class="{'messages__list__item--active': message.user.id == user.id}"
                >
                        <strong class="message__list__item__owner">@{{ message.user.nickname }}</strong>
                        <p>@{{ message.created_ago }}</p>
                        <br>
                        <p>@{{ message.body }}</p>
                </li>
            </ul>
            <p v-else>
                No messages yet.
            </p>
        </div>

        <div>
            <div class="field">
                <input type="text" class="input" placeholder="Say what you want to say" v-model="message">
            </div>
            <div class="field">
                <button class="button is-info" @click.prevent="sendMessage">
                    Send
                </button> 
            </div>
        </div>
    </div>
@stop

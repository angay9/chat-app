
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('modal', require('./components/modal.vue'));

$(function () {
    $(".main").animate({ scrollTop: $(".main").height() }, 0);
});

const app = new Vue({
    el: '#app',
    /**
     * Get vue instance data
     * 
     * @return {Object}
     */
    data() {
        return {
            user: window.Laravel.user,
            nickname: '',
            channels: window.Laravel.channels,
            selectedChannel: window.Laravel.channels.length > 0 ? window.Laravel.channels[0] : null,
            message: ''
        }
    },
    /**
     * Mounted callback
     * 
     * @return {vodi}
     */
    mounted() {
        this.listen();
    },
    methods: {
        /**
         * Select a channel
         * 
         * @param  {Object} channel
         * @return {void}
         */
        selectChannel(channel) {
            this.selectedChannel = channel;
            this.$nextTick(() => {
                $(".main").animate({ scrollTop: $(".main").height() }, 0);
            });
        },
        
        /**
         * Send message
         * 
         * @return {void}
         */
        sendMessage() {
            axios.post('/messages', {
                'message': this.message,
                'channelId': this.selectedChannel.id,
            }).then((res) => {
                this.message = '';
                this.selectedChannel.messages.push(res.data.message);
            }).catch((res) => {
                alert('Error. Please try again later.');
            })
        },

        /**
         * Listen to the socket events
         * 
         * @return {void}
         */
        listen() {
            for (channel of this.channels) {
                Echo.join(`channel.${channel.id}`)
                    .listen('MessageWasCreatedEvent', (e) => {
                        // console.log(e);
                        let message = e.message;
                        message.user = e.user;

                        this.channels.forEach(c => {
                            if (c.id == e.message.channel_id) {
                                c.messages.push(message);      
                            }
                        })
                    })
                ;
            }
        }
    }
});

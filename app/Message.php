<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['channel_id', 'body', 'user_id'];

    public $appends = ['created_ago'];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationship Messages
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship Messages
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
     * Get created_ago attribute as "human" string
     * 
     * @return string
     */
    public function getCreatedAgoAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}

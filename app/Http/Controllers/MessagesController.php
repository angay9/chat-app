<?php

namespace App\Http\Controllers;

use App\Events\MessageWasCreatedEvent;
use App\Message;
use Auth;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    /**
     * Store a message
     * 
     * @param  Request $request
     * @return array
     */
    public function store(Request $request)
    {
        abort_unless(Auth::check(), 401);

        $this->validate($request, [
            'message'   =>  'required',
            'channelId' => 'required|exists:channels,id'
        ]);

        $message = new Message([
            'channel_id'    =>  $request->get('channelId'),
            'user_id'    =>  Auth::user()->id,
            'body'   =>  $request->get('message')
        ]);

        $message->save();
        $message = $message->fresh(['user', 'channel']);
        // $message->user;

        broadcast(new MessageWasCreatedEvent($message, Auth::user()))->toOthers();

        return [
            'success'   =>  true,
            'message'   =>  $message
        ];
    }
}

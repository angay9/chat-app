<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show home view
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $channels = Channel::with(['messages' => function ($q) {
                    $q->orderBy('created_at', 'ASC');
                }, 'messages.user']
            )
            ->get()
        ;

        return view('index', [
            'channels'  => $channels
        ]);
    }
}
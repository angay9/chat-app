<?php

namespace App\Http\Controllers;

use App\Events\UserLoggedInEvent;
use App\User;
use Auth;
use Socialize;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Redirect to provider
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function redirectToProvider()
    {
        return Socialize::with('facebook')->redirect();
    }

    /**
     * Handle provider callback
     * 
     * @return Illuminate\Http\RecirectResponse
     */
    public function handleProviderCallback()
    {
        $facebookUser = Socialize::with('facebook')->user();
        $user = User::firstOrNew(['email' => $facebookUser->getEmail()]);
 
        if (!$user->exists) {
            $user->email = $facebookUser->getEmail();
            $user->nickname = $facebookUser->nickname ? : $facebookUser->name;
            $user->save();
        }

        Auth::login($user);

        event(new UserLoggedInEvent($user));

        return redirect('/');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        auth()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

}

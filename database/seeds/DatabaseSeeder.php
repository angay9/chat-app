<?php

use App\Channel;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channels = ['Main', 'Programming', 'Design'];

        foreach ($channels as $channel) {
            Channel::create(['name' => $channel]);            
        }


        // $this->call(UsersTableSeeder::class);
    }
}
